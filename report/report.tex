\documentclass[a4paper, 10pt]{article}
\usepackage{mathpazo}
\usepackage{amsmath}	
\usepackage{amssymb}	
\usepackage{mathrsfs}
\usepackage{amsthm}
\usepackage{subfig}
\usepackage{placeins}
\usepackage{minted}
\usepackage{tikz}
\usepackage{stmaryrd}
\usepackage[top=2cm, bottom=2cm, right=2cm, left=2cm]{geometry}

\author{Marc \textsc{Chevalier}}
\date{\today}
\title{Modelling the \textsc{Stroop} effect with a \textsc{Hopfield} network}

\newcommand{\C}{\mathcal{C}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\set}[1]{{\left\lbrace #1\right\rbrace}}

\begin{document}

\maketitle

\section{Standard \textsc{Hopfield} Network}

\subsection{Getting started}

I wrote an abstract class \texttt{HopfieldNetwork} to provide base methods for network simulation. There is only one pure virtual method, \texttt{\_run}, which compute the next state. The constructor of this class takes one argument: the list of pattern to learn as a \texttt{numpy.array} wrapped in a utility class. This class is inherited by \texttt{DeterministicAsynchronousHopfieldNetwork} (among others we will use latter). This class implement \texttt{\_run} as the asynchronous update. The constructor takes one additional argument: the order to update the neuron of the network. There is 3 modes: in the natural order, in the reverse order or with respect to a random permutation picked at each step. The natural order is interesting for the first exercise. Since all values are indistinguishable, the order has no importance, moreover, the default iteration fashion is faster. Moreover, I found this discussion interesting, so, sometime I will compare the three traversing modes.

The other reason to look at the natural traversing is that we can adapt the $W$ matrix to simulate the asynchronous update with natural order in a synchronous update. Then, to simulate asynchronous update in a random order, we just have to permute components before the multiplication by $W$ and use the reciprocal permutation after. The idea is to have an higher cost at the creation of $W$ and then use efficient matrix-vector multiplication at each iteration. This is efficient only if we do enough iterations. But this modified $W$ matrix is pretty hard to compute. In fact, at the coordinates $(i,j)$ we can prove by induction that this is the term of all products of terms of the kind $w_{i_0, i_1} w_{i_1, i_2} \ldots w_{i_{k-2}, i_{k-1}}w_{i_{k-1}, i_k}$ where $i_0 = i$, $i_k = j$ and $\forall m\in\llbracket 0,k-2 \rrbracket, i_{m} > i_{m+1}$. This is possible by dynamic programming, but not very interesting and probably to difficult for this case. The computations time are reasonnable without this optimization. However, I would have done it if we use really larger network and number of iterations.

The halting condition is the reaching of a minimum of energy. Indeed, for each neuron we consider, we flip it iff it will make the energy decrease. So, since the network have not reached a minimum of energy, it will evolve. Reciprocally, once a minimum reached, the network is at a fixed point. The energy is defined as $-\frac{1}{2}S^t W S$, but for the sake of simplicity (and avoid rounding error), I use $-S^t W S$ in the code.

This class is covered by some unitary tests showing it works correctly.

To avoid having magic constant in the code, I use Json files for the configuration of each experiment. It's just technical, not interesting.

All plots are made using \texttt{gnuplot}. The only reason, I choose this over its many competitors is that I know \texttt{gnuplot} way better.

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=1]{../questions/q11.pdf}
    \caption{Evolution in time of the overlap between the network state initialized with a disrupted pattern and the desired pattern}\label{q11}
\end{figure}

\FloatBarrier

As the figure \ref{q11} shows, the convergence is very fast. The implementation seems to be correct.

\subsection{Normalized pixel distance}

We have
\[
    \xi_i^\mu S_i(t) =
    \begin{cases}
        1 & \xi_i^\mu = S_i(t)\\
        -1 & \xi_i^\mu \neq S_i(t)
    \end{cases}
\]
so
\[
    \frac{-\xi_i^\mu S_i(t)+1}{2} =
    \begin{cases}
        0 & \xi_i^\mu = S_i(t)\\
        1 & \xi_i^\mu \neq S_i(t)
    \end{cases}
\]
The total of incorrect pixel is
\[
    \sum\limits_{i=1}^N \frac{-\xi_i^\mu S_i(t)+1}{2}
\]
and the percentage (the mean retrieval error) is 
\[
    \begin{aligned}
        \frac{1}{N}\sum\limits_{i=1}^N \frac{-\xi_i^\mu S_i(t)+1}{2} &= \frac{1}{2}-\frac{1}{2N}\sum\limits_{i=1}^N \xi_i^\mu S_i(t)\\
        &= \frac{1}{2}-\frac{1}{2}\frac{1}{N}\left\langle \xi^\mu \middle\vert S(t) \right\rangle\\
        &= \frac{1}{2}-\frac{1}{2}m^\mu
    \end{aligned}
\]

\subsection{Pattern retrieval}

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=1]{../questions/q131.pdf}
    \caption{Mean retrieval error of a randomly chosen pattern with $N=200$ and $P=5$}\label{q131}
\end{figure}

\FloatBarrier

For $c \leqslant 0.30$, the mean retrieval error seems to be below 2\%.

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=1]{../questions/q132.pdf}
    \caption{Mean retrieval error of a randomly chosen pattern with $N=200$ and $c=0.1$}\label{q132}
\end{figure}

\FloatBarrier

For $P \leqslant 20$, the mean retrieval error seems to be below 2\%.

\subsection{Capacity estimation}

For this question, I do not use a linear search but a binary search since the predicate is monotonic and we have a bound on the number of stored patterns since we can not store more pattern than the number of neurons.

After some computations, we get
\begin{itemize}
    \item $N=100$
        \[
            \begin{aligned}
                \mu &= 0.26\\
                \sigma &= 0.02981423969999719
            \end{aligned}
        \]
    \item $N=250$
        \[
            \begin{aligned}
                \mu &= 0.1988\\
                \sigma &= 0.011003029885748139
            \end{aligned}
        \]
    \item $N=500$
        \[
            \begin{aligned}
                \mu &= 0.1802\\
                \sigma &= 0.005028805910838967
            \end{aligned}
        \]
\end{itemize}

The capacity seems to decrease to $n$. In the literature, I found that the mean capacity is approximatively 14\%. My results are a bit higher, but not so much. We can believe that if we make the computation for bigger $N$, capacity will converge to $0.14$ Furthermore, the standard deviation decrease.

\section{Modelling the \textsc{Stroop} effect}

\subsection{Getting started}

Here, the network implementation do not change. The only difference is the wrapper used for the patterns which is inherited from the \textsc{Pattern} wrapper. This wrapper provides a static method to generate \textsc{Stroop} patterns, 2 getters (for congruent and incongruent patterns) and a function to decide the success using the configuration and the indices of the word part and the color part. Theses indices are returned by the getters. If we provide wrong indices, the wrapper do not attempt to detect or correct it.

We have the following results
\begin{verbatim}
Result for congruent patterns (Default):
    100.0% of success in mean time 0.0 (std = 0.0)
Result for congruent patterns (Reverse):
    100.0% of success in mean time 0.0 (std = 0.0)
Result for congruent patterns (Random):
    100.0% of success in mean time 0.0 (std = 0.0)
Result for non congruent patterns (Default):
    91.10000000000001% of success in mean time 1.838 (std = 0.5329540693531104)
Result for non congruent patterns (Reverse):
    9.0% of success in mean time 1.806 (std = 0.5240597662849522)
Result for non congruent patterns (Random):
    50.2% of success in mean time 1.677 (std = 0.7008296242041728)
\end{verbatim}

Unsurprisingly, for congruent patterns, nothing happen, everything goes well! But for incongruent patterns, we have a success close to 50\%, which is perfectly understandable since the color-part and the word-part have the same weight: we have the same chances to converge to each.

An interesting observation, is that if we use the default or reverse iteration, we have respectively an high and low success rate. The explication is very simple: if we begin by update the word-part, it will fit the color-part, and reciprocally. Once arrived to the other part, the network is close enough to a congruent pattern to converge to it. This is interesting because, if, for example, we know (even approximatively) where the network has the more chance to be incorrect, by updating these neurons first, we can increase the rate of success.

\subsection{Impact of $k$}

A subject has difficulty to say to color of ink, but not to say the word. So, we can think the weight of the word is higher, corresponding to a low $k$.

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=1]{../questions/q22_error.pdf}
    \caption{Number of errors of the subject}\label{q22_error}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=1]{../questions/q22_overlap.pdf}
    \caption{Mean overlap of the final state with the corresponding congruent pattern}\label{q22_overlap}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=1]{../questions/q22_time.pdf}
    \caption{Mean number of time steps until convergence}\label{q22_time}
\end{figure}

\FloatBarrier

We see that, when the $k$ is low, the error rate is important (like for a human subject). But a human subject, even in stress situation (timed test) do not fail so often. Let's assume, he fails one time over two. This correspond to $k\approx 0.5$. We observe that in this case, the response time is higher ($\sim 1.6$) than when we begin from a congruent pattern ($= 1$). It correspond to a human subject who hesitate and take more time to answer. The overlap has the expected shape, but I do not see any concrete interpretation.

When $k=0.5$, we are close to a critical point of the energy as a function of the state of the network. So, we are farther of the minima, moreover, since the energy is a quadratic form, it is smooth ($\C^\infty(\RR^N, \RR)$ in fact), so the gradient close to a critical point is pretty small: the energy will not evolve very quickly. So, the response time is higher. The error probability come from the probability to fall to a minimum or another exactly like drainage divide.

\subsection{Impact of the number of color}

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=1]{../questions/q23_error.pdf}
    \caption{Number of errors of the subject}\label{q23_error}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=1]{../questions/q23_overlap.pdf}
    \caption{Mean overlap of the final state with the corresponding congruent pattern}\label{q23_overlap}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=1]{../questions/q23_time.pdf}
    \caption{Mean number of time steps until convergence}\label{q23_time}
\end{figure}

\FloatBarrier

Unsurprisingly, an increase of the number of colors induce an increasing of the number of errors and the reaction time. We can find the same behaviour in a human subject.

Indeed, by setting more pattern,we add minima and so flatten the quadratic form of the energy in the interesting region. Consequently, the convergence is slower and we have more chances to converge to the wrong minima.

\subsection{Asynchronous vs synchronous update}

To implement synchronous update, I simply inherited the base class \texttt{HopfieldNetwork} into \texttt{Deterministic}-\texttt{SynchronousHopfieldNetwork} to reimplement \texttt{\_run}. This time, it consists simply in a matrix multiplication.

The results are
\begin{verbatim}
Result for congruent patterns:
    100.0% of success in mean time 0.0 (std = 0.0)
Result for non congruent patterns:
    53.2% of success in mean time 2.377 (std = 1.007912142444413)
\end{verbatim}

A comfortable fact, the network terminate immediately when initialized with a congruent pattern.

When we begin with an incongruent pattern, the success rate is roughly the same that for an asynchronous network, but the converge time, is higher and more variable.

\subsection{Speculation}

When a subject is concentrated, he thinks about what he have to do, and so, will make his answer more easily influenced by the observations based on the ink color than on the word. To simulate this, we can use a non symmetric weight matrix: $w_{i,j}$ will be higher than $w_{j,i}$ when $i$ belong to the word-part and $j$ to the color-part. Thereby, the color will have higher influence on the word than the reciprocal.

\subsection{Stochastic model}

I do not find better halting condition. I use the same.

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=1]{../questions/q26_error.pdf}
    \caption{Number of errors of the subject}\label{q26_error}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=1]{../questions/q26_overlap.pdf}
    \caption{Mean overlap of the final state with the corresponding congruent pattern}\label{q26_overlap}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=1]{../questions/q26_time.pdf}
    \caption{Mean number of time steps until convergence}\label{q26_time}
\end{figure}

\FloatBarrier

When $\beta$ is low, the thermal noise is more intense, and the error rate is more important. There is one of the problem of the halting condition: because of the noise, we do not have the monotony of the energy. Moreover, we could use this non monotonicity to go out of a local shallow accidental minimum to reach another deeper minimum which correspond to a learned pattern. It would be similar to the simulated annealing, a method to avoid being trapped in local minimum to find better minimum. But I didn't succeed to take advantage of this possibility.

\end{document}