import numpy
from unittest import TestCase
from hopfield.hopfield_network import DeterministicAsynchronousHopfieldNetwork as HopfieldNetwork
from hopfield.hopfield_network import TraversingMode
from hopfield.states import Pattern, NetworkConfiguration


class TestHopfieldNetwork(TestCase):
    def testInit(self):
        patterns = Pattern(numpy.array([[1, 1, 1, 1, 1], [-1, -1, -1, -1, 1]]))
        net = HopfieldNetwork(patterns, TraversingMode.Default)
        self.assertEqual(net.patterns_number, 2)
        self.assertEqual(net.size, 5)
        self.assertEqual(net.weights.shape, (5, 5))
        for i in range(net.size):
            self.assertEqual(net.weights[i][i], 0, "The %s-th component on the diagonal is non 0" % i)
        self.assertEqual(net.config, None)

    def testNext(self):
        patterns = Pattern(numpy.array([[1, 1, 1, 1, 1], [-1, -1, -1, -1, 1]]))
        net = HopfieldNetwork(patterns, TraversingMode.Default)
        net.set_config(NetworkConfiguration(numpy.array([1, 1, -1, 1, 1])))
        net._next()
        for i in range(net.size):
            self.assertEqual(net.config.states[i], 1)

    def testEnergy(self):
        patterns = Pattern(numpy.array([[1, 1, 1, 1, 1], [-1, -1, -1, -1, 1]]))
        net = HopfieldNetwork(patterns, TraversingMode.Default)
        net.set_config(NetworkConfiguration(numpy.array([1, 1, -1, 1, 1])))
        self.assertAlmostEqual(net._energy(), 0, delta=0.01)
        net._next()
        self.assertAlmostEqual(net._energy(), -24/5, delta=0.01)
        net._next()
        self.assertAlmostEqual(net._energy(), -24 / 5, delta=0.01)

    def testRun(self):
        patterns = Pattern(numpy.array([[1, 1, 1, 1, 1], [-1, -1, -1, -1, 1]]))
        net = HopfieldNetwork(patterns, TraversingMode.Default)
        net.set_config(NetworkConfiguration(numpy.array([1, 1, -1, 1, 1])))
        self.assertAlmostEqual(net._energy(), 0, delta=0.01)
        net.run()
        self.assertAlmostEqual(net._energy(), -24 / 5, delta=0.01)

    def testOverlap(self):
        patterns = Pattern(numpy.array([[1, 1, 1, 1, 1], [-1, -1, -1, -1, 1]]))
        net = HopfieldNetwork(patterns, TraversingMode.Default)
        pattern = NetworkConfiguration(numpy.array([1, 1, 1, 1, 1]))
        net.set_config(NetworkConfiguration(numpy.array([1, 1, -1, 1, 1])))
        self.assertAlmostEqual(net.overlap(pattern), 3/5, delta=0.01)
        net.run()
        self.assertAlmostEqual(net.overlap(pattern), 1, delta=0.01)
