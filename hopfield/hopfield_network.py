"""Network module"""

from enum import Enum
from abc import ABCMeta, abstractmethod
import numpy as np
from .states import Pattern, NetworkConfiguration


class HopfieldNetwork(metaclass=ABCMeta):
    """
    Store and simulate an Hopfield network
    """
    def __init__(self, patterns: Pattern) -> None:
        self.patterns_number, self.size = patterns.shape()
        assert self.patterns_number > 0
        assert self.size > 0
        self.weights = (patterns.transpose().dot(patterns).states -
                        self.patterns_number * np.eye(self.size)) / self.size
        self.config = None
        self.step = 0

    def set_config(self, config: NetworkConfiguration) -> None:
        """
        Set the state of the network
        :param config: A vector as a numpy.ndarray which represent the
         configuration of the network.
        :return: None
        """
        self.config = config
        self.step = 0

    @abstractmethod
    def _next(self) -> None:
        """
        Compute the next state
        :return: None
        """
        pass

    def steps(self, step: int=1) -> None:
        """
        Compute 'step' steps of the network
        :param step: An integer, the number of steps to compute
        :return: None
        """
        for _ in range(step):
            self._next()

    def _energy(self) -> np.float64:
        """
        Get the energy of the network
        :return: numpy.float64
        """
        # Actually, the energy is -\frac{1}{2} S^t W S. But I don't need constant factors.
        return -np.dot(self.config.transpose().states, np.dot(self.weights, self.config.states))

    def run(self) -> None:
        """
        Compute steps of the network until stabilization
        :return: None
        """
        energy = self._energy()
        self._next()
        energy_ = self._energy()
        while energy > energy_:
            energy = energy_
            self._next()
            energy_ = self._energy()
        self.step -= 1

    def get_config(self) -> NetworkConfiguration:
        """
        Get the current config
        :return: numpy.array
        """
        return self.config

    def overlap(self, pattern: NetworkConfiguration) -> np.float64:
        """
        Compute the overlap with a given pattern
        :param pattern: numpy.array, a pattern
        :return: numpy.float64, the overlap
        """
        return pattern.dot(self.config) / self.size

    def mean_retrieval_error(self, pattern: NetworkConfiguration, c: float) -> np.float64:
        """
        Compute the mean retrieval error of a pattern with a given perturbation probability
        :param pattern: NetworkConfiguration, a pattern to modify and try to retrieve
        :param c: float, the fraction of cell to flip
        :return: numpy.float64, the mean retrieval error when running the network on the modified
            pattern
        """
        config = NetworkConfiguration.flip_randomly(pattern, c)
        self.set_config(config)
        self.run()
        return 0.5-0.5*float(self.overlap(pattern))


def default_range(size: int) -> range:
    """
    :param size: int, the length of the range
    :return: range, the range [0..range-1]
    """
    return range(size)


def random_range(size):
    """
    :param size: int, the length of the range
    :return: range, a permutation of [0..range-1]
    """
    return np.random.permutation(size)


def reverse_range(size):
    """
    :param size: int, the length of the range
    :return: range, the range [range-1..0]
    """
    return reversed(range(size))


class TraversingMode(Enum):
    """
    Describe the 3 ways to traverse the network for the update.
    Default: in the order
    Reverse: from the end to the front
    Random: random order
    """
    Default = default_range
    Reverse = reverse_range
    Random = random_range


class DeterministicAsynchronousHopfieldNetwork(HopfieldNetwork):
    """
    Derived Hopfield network implemented deterministic and asynchronous update.
    Here deterministic apply to the update function, not the order of update.
    """
    def __init__(self, patterns: Pattern, mode):
        super(DeterministicAsynchronousHopfieldNetwork, self).__init__(patterns)
        self.mode = mode

    def _next(self) -> None:
        """
        Compute the next state
        :return: None
        """
        self.step += 1
        traversing_range = self.mode(self.size)
        for j in traversing_range:
            self.config.states[j] = -1 if np.dot(self.weights[j, :], self.config.states) < 0 else 1


class StochasticAsynchronousHopfieldNetwork(HopfieldNetwork):
    """
    Derived Hopfield network implemented stochastic and asynchronous update.
    """
    def __init__(self, patterns: Pattern, mode, beta: float):
        super(StochasticAsynchronousHopfieldNetwork, self).__init__(patterns)
        self.mode = mode
        self.beta = beta

    def _next(self) -> None:
        """
        Compute the next state
        :return: None
        """
        self.step += 1
        traversing_range = self.mode(self.size)
        for j in traversing_range:
            dot = np.dot(self.weights[j, :], self.config.states)
            self.config.states[j] = np.random.binomial(1, 1/(1+np.exp(-dot*self.beta)))*2-1


class DeterministicSynchronousHopfieldNetwork(HopfieldNetwork):
    """
    Derived Hopfield network implemented stochastic and synchronous update.
    """
    def _next(self) -> None:
        """
        Compute the next state
        :return: None
        """
        self.step += 1
        self.config.states = np.sign(np.sign(np.dot(self.weights, self.config.states))+0.5)
