"""Configuration module."""

import os
import json
from abc import ABCMeta, abstractmethod


class InvalidConfig(Exception):
    """Exception raised when there is an error in the config."""
    pass


class BaseConfig(metaclass=ABCMeta):
    """
    Provide base class to load config from json file
    """
    __slots__ = ('debug',)

    def __init__(self, data=None):
        if not hasattr(self, '_get_config_path_variable') or \
                not hasattr(self, 'parse_config'):
            raise NotImplementedError('Config class does not implement all '
                                      'required attributes.')
        self.debug = True
        if not data:
            try:
                with open(self.get_config_path()) as file:
                    data = json.load(file)
            except ValueError as exc:
                raise InvalidConfig(*exc.args)
        self.parse_config(data)

    def get_config_path(self) -> str:
        """
        :return: str, the config path
        """
        path = os.environ.get(self._get_config_path_variable(), '')
        if not path:
            raise InvalidConfig('Could not find config file, please set '
                                'environment variable $%s.' %
                                self._get_config_path_variable())
        return path

    @abstractmethod
    def _get_config_path_variable(self) -> str:
        """
        :return: str, the config path variable
        """
        pass

    @abstractmethod
    def parse_config(self, data) -> None:
        """
        Parse the json AST in variables
        :param data:
        :return: None
        """
        pass


class SimpleConfig(BaseConfig):
    """
    Specialized config for a fixed triple (N, P, c).
    """
    __slots__ = ('N', 'P', 'c')

    def __init__(self, data=None):
        self.N = None
        self.P = None
        self.c = None
        super(SimpleConfig, self).__init__(data)

    def _get_config_path_variable(self) -> str:
        return 'SIMPLE_HOPFIELD_CONFIG'

    def parse_config(self, data) -> None:
        self.N = int(data['N'])
        self.P = int(data['P'])
        self.c = float(data['c'])


class IntervalCConfig(BaseConfig):
    """
    Specialized config for fixed N and P, and a range for c.
    """
    __slots__ = ('N', 'P', 'c_min', 'c_max', 'c_step', 'nb_exp')

    def __init__(self, data=None):
        self.N = None
        self.P = None
        self.c_min = None
        self.c_max = None
        self.c_step = None
        self.nb_exp = None
        super(IntervalCConfig, self).__init__(data)

    def _get_config_path_variable(self) -> str:
        return 'INTERVAL_C_HOPFIELD_CONFIG'

    def parse_config(self, data) -> None:
        self.N = int(data['N'])
        self.P = int(data['P'])
        self.c_min = float(data['c_min'])
        self.c_max = float(data['c_max'])
        self.c_step = float(data['c_step'])
        self.nb_exp = int(data['nb_exp'])


class IntervalPConfig(BaseConfig):
    """
    Specialized config for fixed N and c, and a range for P.
    """
    __slots__ = ('N', 'c', 'P_min', 'P_max', 'P_step', 'nb_exp')

    def __init__(self, data=None):
        self.N = None
        self.c = None
        self.P_min = None
        self.P_max = None
        self.P_step = None
        self.nb_exp = None
        super(IntervalPConfig, self).__init__(data)

    def _get_config_path_variable(self) -> str:
        return 'INTERVAL_P_HOPFIELD_CONFIG'

    def parse_config(self, data) -> None:
        self.N = int(data['N'])
        self.c = float(data['c'])
        self.P_min = int(data['P_min'])
        self.P_max = int(data['P_max'])
        self.P_step = int(data['P_step'])
        self.nb_exp = int(data['nb_exp'])


class Q14Config(BaseConfig):
    """
    Specialized config for q 1.4
    """
    __slots__ = ('Ns', 'c', 'nb_exp', 'error')

    def __init__(self, data=None):
        self.Ns = None
        self.c = None
        self.error = None
        self.nb_exp = None
        super(Q14Config, self).__init__(data)

    def _get_config_path_variable(self) -> str:
        return 'Q_14_HOPFIELD_CONFIG'

    def parse_config(self, data) -> None:
        self.Ns = map(int, data['Ns'])
        self.c = float(data['c'])
        self.error = float(data['error'])
        self.nb_exp = int(data['nb_exp'])


class Q21Config(BaseConfig):
    """
    Specialized config for q 2.1
    """
    __slots__ = ('N', 'P', 'nb_exp', 'k')

    def __init__(self, data=None):
        self.N = None
        self.P = None
        self.k = None
        self.nb_exp = None
        super(Q21Config, self).__init__(data)

    def _get_config_path_variable(self) -> str:
        return 'Q_21_HOPFIELD_CONFIG'

    def parse_config(self, data) -> None:
        self.N = int(data['N'])
        self.P = int(data['P'])
        self.k = float(data['k'])
        self.nb_exp = int(data['nb_exp'])


class IntervalKConfig(BaseConfig):
    """
    Specialized config for fixed N and c, and a range for P.
    """
    __slots__ = ('N', 'P', 'k_min', 'k_max', 'k_step', 'nb_exp', 'nb_trials')

    def __init__(self, data=None):
        self.N = None
        self.P = None
        self.nb_trials = None
        self.k_min = None
        self.k_max = None
        self.k_step = None
        self.nb_exp = None
        super(IntervalKConfig, self).__init__(data)

    def _get_config_path_variable(self) -> str:
        return 'INTERVAL_K_HOPFIELD_CONFIG'

    def parse_config(self, data) -> None:
        self.N = int(data['N'])
        self.P = int(data['P'])
        self.nb_trials = int(data['nb_trials'])
        self.k_min = float(data['k_min'])
        self.k_max = float(data['k_max'])
        self.k_step = float(data['k_step'])
        self.nb_exp = int(data['nb_exp'])


class IntervalPConfig2(BaseConfig):
    """
    Specialized config for fixed N and c, and a range for P.
    """
    __slots__ = ('N', 'k', 'P_min', 'P_max', 'P_step', 'nb_exp', 'nb_trials')

    def __init__(self, data=None):
        self.N = None
        self.k = None
        self.P_min = None
        self.P_max = None
        self.P_step = None
        self.nb_exp = None
        self.nb_trials = None
        super(IntervalPConfig2, self).__init__(data)

    def _get_config_path_variable(self) -> str:
        return 'INTERVAL_P_HOPFIELD_CONFIG_2'

    def parse_config(self, data) -> None:
        self.N = int(data['N'])
        self.k = float(data['k'])
        self.nb_trials = int(data['nb_trials'])
        self.P_min = int(data['P_min'])
        self.P_max = int(data['P_max'])
        self.P_step = int(data['P_step'])
        self.nb_exp = int(data['nb_exp'])


class Q26Config(BaseConfig):
    """
    Specialized config for q 2.6
    """
    __slots__ = ('N', 'P', 'k', 'Betas', 'nb_exp', 'nb_trials')

    def __init__(self, data=None):
        self.N = None
        self.P = None
        self.k = None
        self.Betas = None
        self.nb_exp = None
        self.nb_trials = None
        super(Q26Config, self).__init__(data)

    def _get_config_path_variable(self) -> str:
        return 'Q_26_HOPFIELD_CONFIG'

    def parse_config(self, data) -> None:
        self.Betas = map(float, data['Betas'])
        self.N = int(data['N'])
        self.P = int(data['P'])
        self.k = float(data['k'])
        self.nb_exp = int(data['nb_exp'])
        self.nb_trials = int(data['nb_trials'])
