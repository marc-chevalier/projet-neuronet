"""Module to store configs"""

import random
import numpy as np


class States:
    """
    Base class to store a matrix of +/- 1 which can be interpreted as a list of pattern,
    the state of a network (if the matrix is a vector).
    """
    def __init__(self, states: np.array) -> None:
        self.states = states

    def shape(self):
        """
        :return: the shape of the state.
        """
        return self.states.shape

    def transpose(self):
        """
        Transpose the pattern. Useful for computations.
        :return: the transposed pattern, the object is not modified
        """
        return States(np.transpose(self.states))

    def dot(self, rhs):
        """
        Matrix product
        :param rhs: Another States
        :return: self * rhs
        """
        return States(np.dot(self.states, rhs.states))


class Pattern(States):
    """
    Represent the patterns stored in a network. This subclass is useful to check types. In
    addition, it contains a static method to generate random patterns and a getter to get one of
    the patterns as a NetworkConfiguration.
    """
    def __init__(self, states: np.array) -> None:
        super(Pattern, self).__init__(states)

    @staticmethod
    def generate_random_patterns(size_of_patterns: int, number_of_pattern: int=1) -> np.array:
        """
        Generate number_of_pattern random pattern of size size_of_patterns
        with value in {+1, -1}, uniformly and independently distributed
        :param size_of_patterns: int, the size of patterns
        :param number_of_pattern: int, the number of patterns
        :return: np.array, the patterns
        """
        return Pattern(
            np.sign(
                np.sign(
                    np.random.rand(
                        number_of_pattern,
                        size_of_patterns) - 0.5) + 0.5))

    def __getitem__(self, item: int):
        """
        Get a pattern as a NetworkConfiguration.
        :param item: int, the index of the pattern to get
        :return: NetworkConfiguration, the item-th pattern
        """
        return NetworkConfiguration(self.states[item])


class NetworkConfiguration(States):
    """
    Derived class to represent the configuration of a network. The main plus is an assertion which
    check is the matrix is a vector.
    """
    def __init__(self, states: np.array) -> None:
        assert len(states.shape) == 1
        super(NetworkConfiguration, self).__init__(states)

    def dot(self, rhs):
        """
        Vector product, but more flexible about types.
        :param rhs: A NetworkConfiguration or a vector
        :return: the inner product.
        """
        if isinstance(rhs, NetworkConfiguration):  # Ok, this really ugly...
            return np.dot(self.states, rhs.states)
        else:
            return np.dot(self.states, rhs)

    def __getitem__(self, item: int):
        """
        Get a cell of the network.
        :param item: int, the index of the pattern to get
        :return: the item-th value
        """
        return self.states[item]

    def overlap(self, rhs):
        """
        Compute the overlap between the configuration and another. Lenient with types.
        :param rhs: Another NetworkConfiguration or a vector
        :return: the overlap
        """
        return self.dot(rhs) / self.states.size

    @staticmethod
    def generate_random_config(size: int):
        """
        Generate a random config with coefficients in {+1, -1} independently
        and uniformly distributed.
        :param size: int, the size of the config to generate
        :return: np.array, the generated config
        """
        return NetworkConfiguration(np.sign(np.sign(np.random.rand(1, size) - 0.5) + 0.5))

    @staticmethod
    def flip_randomly(pattern, ratio_to_change: float) -> np.array:
        """
        Generate a config from a pattern by flipping a given ratio of the
        coefficients
        :param pattern: np.array, the pattern to start from
        :param ratio_to_change: float, the ratio of the coefficients to
        flip
        :return: np.array, the generated config
        """
        size = len(pattern.states)
        permutation = np.random.permutation(size)
        elements_to_flip = [-1 if x < ratio_to_change * size else 1 for x in permutation]
        return NetworkConfiguration(np.multiply(elements_to_flip, pattern.states))


class StroopPattern(Pattern):
    """
    Derived class to store a Stroop pattern. It store the list of words and the list of colors as
    two matrices. It is construct with this two matrices but also contains the 'k' and, more
    important, some methods to do useful things.
    """
    def __init__(self, words: np.array, colors: np.array) -> None:
        assert words.shape[0] == colors.shape[0]
        self.nb_pattern = colors.shape[0]
        super(StroopPattern, self).__init__(np.concatenate((words, colors), axis=1))
        self.words = words
        self.colors = colors
        self.k = colors.shape[1] / (words.shape[1] + colors.shape[1])

    def get_congruent_pattern(self) -> (NetworkConfiguration, int, int):
        """
        Get a random congruent pattern.
        :return: (NetworkConfiguration, int, int), the congruent pattern as a NetworkConfiguration
        and two identical integers which are the index of the word and color part.
        """
        index = random.randint(0, self.nb_pattern-1)
        return \
            NetworkConfiguration(np.concatenate((self.words[index], self.colors[index]))),\
            index,\
            index

    def get_non_congruent_pattern(self) -> (NetworkConfiguration, int, int):
        """
        Get a random non congruent pattern.
        :return: (NetworkConfiguration, int, int), the non congruent pattern as a
        NetworkConfiguration and two integers which are the index of the word and color part.
        """
        index_word = random.randint(0, self.nb_pattern-1)
        index_color = random.randint(0, self.nb_pattern-2)
        index_color = index_color if index_color < index_word else index_color + 1
        return NetworkConfiguration(
            np.concatenate(
                (self.words[index_word],
                 self.colors[index_color]))),\
            index_word,\
            index_color

    def is_success(self, state: NetworkConfiguration, index_word: int, index_color: int) -> bool:
        """
        Test if the retrieval is a success.
        :param state: NetworkConfiguration, the state after running the network on the Stroop
        pattern
        :param index_word: int, the index of the original word part
        :param index_color: int, the index of the original color part
        :return: bool, whether the retrieval is a success or not
        """
        color_overlap = state.overlap(
            np.concatenate(
                (
                    self.words[index_color],
                    self.colors[index_color])))
        word_overlap = state.overlap(
            np.concatenate(
                (
                    self.words[index_word],
                    self.colors[index_word])))
        return color_overlap >= word_overlap, color_overlap

    @staticmethod
    def generate_random_stroop_patterns(size_of_patterns: int, k: float, number_of_pattern: int=1):
        """
        Generate a random Stroop pattern
        :param size_of_patterns: int, the total length of the patterns
        :param k: float, the ratio of the length of the color part with respect to the total length
        :param number_of_pattern: int, the number of pattern to generate.
        :return: The random Stroop pattern
        """
        words = Pattern.generate_random_patterns(
            int(size_of_patterns - size_of_patterns*k),
            number_of_pattern)
        colors = Pattern.generate_random_patterns(int(size_of_patterns*k), number_of_pattern)
        return StroopPattern(words.states, colors.states)
