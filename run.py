#!/usr/bin/env python3

import questions.q11
import questions.q131
import questions.q132
import questions.q14
import questions.q21
import questions.q22
import questions.q23
import questions.q24
import questions.q26
# import cProfile


def main():
    # questions.q11.run()
    # questions.q131.run()
    # questions.q132.run()
    # cProfile.run('graphs.q14.run()') # Alas, I can't do anything...
    # questions.q14.run()
    # questions.q21.run()
    # questions.q22.run()
    # questions.q23.run()
    questions.q24.run()
    # questions.q26.run()

if __name__ == '__main__':
    main()
