#!/usr/bin/env python3

from hopfield.hopfield_network import DeterministicAsynchronousHopfieldNetwork as HopfieldNetwork
from hopfield.hopfield_network import TraversingMode
from hopfield.config import SimpleConfig as Config
from hopfield.states import NetworkConfiguration, Pattern
import os
import progressbar


def run():
    print("Question 1.1")
    os.environ['SIMPLE_HOPFIELD_CONFIG'] = 'questions/q11.json'

    open('questions/q11.dat', 'w').close()
    file = os.open("questions/q11.dat", os.O_WRONLY | os.O_CREAT)

    max_simulations = 10
    max_step = 10
    overlap = []

    os.write(file, b"step ")

    for simulationNumber in range(max_simulations):
        os.write(file, ("%s " % simulationNumber).encode('utf-8'))

    os.write(file, b"\n")

    bar = progressbar.ProgressBar()

    for simulationNumber in bar(range(max_simulations)):
        overlap.append([])
        patterns = Pattern.generate_random_patterns(Config().N, Config().P)
        config = NetworkConfiguration.flip_randomly(patterns[0], Config().c)
        net = HopfieldNetwork(patterns, TraversingMode.Default)
        net.set_config(config)
        for step in range(max_step):
            overlap[simulationNumber].append(net.overlap(patterns[0]))
            net.steps(1)

    for step in range(max_step):
        os.write(file, ("%s " % step).encode('utf-8') + b" ")
        for simulationNumber in range(max_simulations):
            os.write(file, ("%s " % overlap[simulationNumber][step]).encode('utf-8')+b" ")
        os.write(file, b"\n")

    os.close(file)

    os.popen("gnuplot -persist questions/q11.p")
