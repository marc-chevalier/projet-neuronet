#!/usr/bin/env python3

from hopfield.hopfield_network import DeterministicSynchronousHopfieldNetwork as HopfieldNetwork
from hopfield.config import Q21Config as Config
from hopfield.states import StroopPattern
import os
import statistics
import progressbar


def one_exp(member) -> (bool, int):
    patterns = StroopPattern.generate_random_stroop_patterns(Config().N, Config().k, Config().P)
    conf, index_word, index_color = member(patterns)
    net = HopfieldNetwork(patterns)
    net.set_config(conf)
    net.run()
    return patterns.is_success(net.get_config(), index_word, index_color)[0], net.step


def mean_and_std(l):
    mean = statistics.mean(l)
    return mean, statistics.stdev(l, mean)


def run():
    print("Question 2.4")
    os.environ['Q_21_HOPFIELD_CONFIG'] = 'questions/q21.json'  # Yep, same config
    congruent_results = []
    congruent_times = []
    non_congruent_results = []
    non_congruent_times = []

    bar = progressbar.ProgressBar()
    for _ in bar(range(Config().nb_exp)):
        success, step = one_exp(StroopPattern.get_congruent_pattern)
        congruent_results.append(success)
        congruent_times.append(step)
        success, step = one_exp(StroopPattern.get_non_congruent_pattern)
        non_congruent_results.append(success)
        non_congruent_times.append(step)

    mean, std = mean_and_std(congruent_times)
    print("Result for congruent patterns: %s%% of success in mean time %s (std = %s)" %
          (statistics.mean(map(int, congruent_results))*100, mean, std))
    mean, std = mean_and_std(non_congruent_times)
    print("Result for non congruent patterns: %s%% of success in mean time %s (std = %s)" %
          (statistics.mean(map(int, non_congruent_results))*100, mean, std))
