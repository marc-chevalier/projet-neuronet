#!/usr/bin/env python3

from hopfield.config import IntervalKConfig as Config
from hopfield.hopfield_network import DeterministicAsynchronousHopfieldNetwork as HopfieldNetwork
from hopfield.states import StroopPattern
from hopfield.hopfield_network import TraversingMode
import os
import statistics
import progressbar
import numpy


def exp(k: float):
    error = [0, 0, 0]
    time = [[], [], []]
    total_overlap = [[], [], []]
    traversing_mode = [TraversingMode.Default, TraversingMode.Reverse, TraversingMode.Random]
    for i in range(3):
        for _ in range(Config().nb_exp):
            patterns = StroopPattern.generate_random_stroop_patterns(Config().N, k, Config().P)
            net = HopfieldNetwork(patterns, traversing_mode[i])
            for __ in range(Config().nb_trials):
                conf, index_word, index_color = patterns.get_non_congruent_pattern()
                net.set_config(conf)
                net.run()
                time[i].append(net.step)
                success, overlap = patterns.is_success(net.get_config(), index_word, index_color)
                total_overlap[i].append(overlap)
                if not success:
                    error[i] += 1

    return total_overlap, time, list(map(lambda x: x/(Config().nb_exp*Config().nb_trials), error))


def mean_and_std(l):
    mean = statistics.mean(l)
    return mean, statistics.stdev(l, mean)


def run():
    print("Question 2.2")
    os.environ['INTERVAL_K_HOPFIELD_CONFIG'] = 'questions/q22.json'

    open('questions/q22_overlap.dat', 'w').close()
    file_overlap = os.open("questions/q22_overlap.dat", os.O_WRONLY | os.O_CREAT)
    open('questions/q22_time.dat', 'w').close()
    file_time = os.open("questions/q22_time.dat", os.O_WRONLY | os.O_CREAT)
    open('questions/q22_error.dat', 'w').close()
    file_error = os.open("questions/q22_error.dat", os.O_WRONLY | os.O_CREAT)

    os.write(file_overlap, b"k overlap_default std_default overlap_reverse std_reverse overlap_random std_random\n")
    os.write(file_time, b"k time_default std_default time_reverse std_reverse time_random std_random\n")
    os.write(file_error, b"k error_default error_reverse error_random\n")

    bar = progressbar.ProgressBar()
    for k in bar(numpy.arange(Config().k_min, Config().k_max+Config().k_step, Config().k_step)):
        overlap, time, error = exp(k)
        os.write(file_overlap, ("%s " % k).encode('utf-8') + b" " + ("%s %s" % mean_and_std(overlap[0])).encode('utf-8'))
        os.write(file_overlap, b" " + ("%s %s " % mean_and_std(overlap[1])).encode('utf-8'))
        os.write(file_overlap, b" " + ("%s %s\n" % mean_and_std(overlap[2])).encode('utf-8'))
        os.write(file_time, ("%s " % k).encode('utf-8') + b" " + ("%s %s" % mean_and_std(time[0])).encode('utf-8'))
        os.write(file_time, b" " + ("%s %s " % mean_and_std(time[1])).encode('utf-8'))
        os.write(file_time, b" " + ("%s %s\n" % mean_and_std(time[2])).encode('utf-8'))
        os.write(file_error, ("%s" % k).encode('utf-8') + b" " + ("%s" % error[0]).encode('utf-8'))
        os.write(file_error, b" " + ("%s" % error[1]).encode('utf-8'))
        os.write(file_error, b" " + ("%s\n" % error[2]).encode('utf-8'))

    os.close(file_overlap)
    os.close(file_time)
    os.close(file_error)

    os.popen("gnuplot -persist questions/q22_overlap.p")
    os.popen("gnuplot -persist questions/q22_time.p")
    os.popen("gnuplot -persist questions/q22_error.p")
