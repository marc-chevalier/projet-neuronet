#!/usr/bin/env python3

from hopfield.config import Q26Config as Config
from hopfield.hopfield_network import StochasticAsynchronousHopfieldNetwork as HopfieldNetwork
from hopfield.hopfield_network import TraversingMode
from hopfield.states import StroopPattern
import os
import statistics
import progressbar


def exp(beta: float):
    error = [0, 0, 0]
    time = [[], [], []]
    total_overlap = [[], [], []]
    traversing_mode = [TraversingMode.Default, TraversingMode.Reverse, TraversingMode.Random]
    bar = progressbar.ProgressBar()
    for _ in bar(range(Config().nb_exp)):
        for i in range(3):
            patterns = StroopPattern.generate_random_stroop_patterns(Config().N, Config().k, Config().P)
            net = HopfieldNetwork(patterns, traversing_mode[i], beta)
            for __ in range(Config().nb_trials):
                conf, index_word, index_color = patterns.get_non_congruent_pattern()
                net.set_config(conf)
                net.run()
                time[i].append(net.step)
                success, overlap = patterns.is_success(net.get_config(), index_word, index_color)
                total_overlap[i].append(overlap)
                if not success:
                    error[i] += 1

    return total_overlap, time, list(map(lambda x: x/(Config().nb_exp*Config().nb_trials), error))


def mean_and_std(l):
    mean = statistics.mean(l)
    return mean, statistics.stdev(l, mean)


def run():
    print("Question 2.6")
    os.environ['Q_26_HOPFIELD_CONFIG'] = 'questions/q26.json'

    open('questions/q26_overlap.dat', 'w').close()
    file_overlap = os.open("questions/q26_overlap.dat", os.O_WRONLY | os.O_CREAT)
    open('questions/q26_time.dat', 'w').close()
    file_time = os.open("questions/q26_time.dat", os.O_WRONLY | os.O_CREAT)
    open('questions/q26_error.dat', 'w').close()
    file_error = os.open("questions/q26_error.dat", os.O_WRONLY | os.O_CREAT)

    os.write(file_overlap, b"beta overlap_default std_default overlap_reverse std_reverse overlap_random std_random\n")
    os.write(file_time, b"beta time_default std_default time_reverse std_reverse time_random std_random\n")
    os.write(file_error, b"beta error_default error_reverse error_random\n")

    for beta in Config().Betas:
        print("\u03b2 = %s" % beta)
        overlap, time, error = exp(beta)
        os.write(file_overlap, ("%s " % beta).encode('utf-8') + b" " + ("%s %s" % mean_and_std(overlap[0])).encode('utf-8'))
        os.write(file_overlap, b" " + ("%s %s " % mean_and_std(overlap[1])).encode('utf-8'))
        os.write(file_overlap, b" " + ("%s %s\n" % mean_and_std(overlap[2])).encode('utf-8'))
        os.write(file_time, ("%s " % beta).encode('utf-8') + b" " + ("%s %s" % mean_and_std(time[0])).encode('utf-8'))
        os.write(file_time, b" " + ("%s %s " % mean_and_std(time[1])).encode('utf-8'))
        os.write(file_time, b" " + ("%s %s\n" % mean_and_std(time[2])).encode('utf-8'))
        os.write(file_error, ("%s" % beta).encode('utf-8') + b" " + ("%s" % error[0]).encode('utf-8'))
        os.write(file_error, b" " + ("%s" % error[1]).encode('utf-8'))
        os.write(file_error, b" " + ("%s\n" % error[2]).encode('utf-8'))

    os.close(file_overlap)
    os.close(file_time)
    os.close(file_error)

    os.popen("gnuplot -persist questions/q26_overlap.p")
    os.popen("gnuplot -persist questions/q26_time.p")
    os.popen("gnuplot -persist questions/q26_error.p")
