#!/usr/bin/env python3

from hopfield.hopfield_network import DeterministicAsynchronousHopfieldNetwork as HopfieldNetwork
from hopfield.hopfield_network import TraversingMode
from hopfield.config import Q21Config as Config
from hopfield.states import StroopPattern
import os
import statistics
import progressbar


def one_exp(member, traversing_mode) -> (bool, int):
    patterns = StroopPattern.generate_random_stroop_patterns(Config().N, Config().k, Config().P)
    conf, index_word, index_color = member(patterns)
    net = HopfieldNetwork(patterns, traversing_mode)
    net.set_config(conf)
    net.run()
    return patterns.is_success(net.get_config(), index_word, index_color)[0], net.step


def mean_and_std(l):
    mean = statistics.mean(l)
    return mean, statistics.stdev(l, mean)


def run():
    print("Question 2.1")
    os.environ['Q_21_HOPFIELD_CONFIG'] = 'questions/q21.json'
    congruent_results = [[], [], []]
    congruent_times = [[], [], []]
    non_congruent_results = [[], [], []]
    non_congruent_times = [[], [], []]

    traversing_mode = [TraversingMode.Default, TraversingMode.Reverse, TraversingMode.Random]

    bar = progressbar.ProgressBar()
    for _ in bar(range(Config().nb_exp)):
        for i in range(3):
            success, step = one_exp(StroopPattern.get_congruent_pattern, traversing_mode[i])
            congruent_results[i].append(success)
            congruent_times[i].append(step)
            success, step = one_exp(StroopPattern.get_non_congruent_pattern, traversing_mode[i])
            non_congruent_results[i].append(success)
            non_congruent_times[i].append(step)

    mean, std = mean_and_std(congruent_times[0])
    print("Result for congruent patterns (Default): %s%% of success in mean time %s (std = %s)" %
          (statistics.mean(map(int, congruent_results[0]))*100, mean, std))
    mean, std = mean_and_std(congruent_times[1])
    print("Result for congruent patterns (Reverse): %s%% of success in mean time %s (std = %s)" %
          (statistics.mean(map(int, congruent_results[1]))*100, mean, std))
    mean, std = mean_and_std(congruent_times[2])
    print("Result for congruent patterns (Random): %s%% of success in mean time %s (std = %s)" %
          (statistics.mean(map(int, congruent_results[2]))*100, mean, std))
    mean, std = mean_and_std(non_congruent_times[0])
    print("Result for non congruent patterns (Default): %s%% of success in mean time %s (std = %s)" %
          (statistics.mean(map(int, non_congruent_results[0]))*100, mean, std))
    mean, std = mean_and_std(non_congruent_times[1])
    print("Result for non congruent patterns (Reverse): %s%% of success in mean time %s (std = %s)" %
          (statistics.mean(map(int, non_congruent_results[1]))*100, mean, std))
    mean, std = mean_and_std(non_congruent_times[2])
    print("Result for non congruent patterns (Random): %s%% of success in mean time %s (std = %s)" %
          (statistics.mean(map(int, non_congruent_results[2]))*100, mean, std))
