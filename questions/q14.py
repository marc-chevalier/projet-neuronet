#!/usr/bin/env python3

from hopfield.hopfield_network import DeterministicAsynchronousHopfieldNetwork as HopfieldNetwork
from hopfield.hopfield_network import TraversingMode
from hopfield.config import Q14Config as Config
from hopfield.states import Pattern
import os
import statistics
import progressbar


def is_storable(n: int, error: float, p: int) -> bool:
    patterns = Pattern.generate_random_patterns(n, p)
    net = HopfieldNetwork(patterns, TraversingMode.Default)
    l = [net.mean_retrieval_error(pattern, Config().c) for pattern in patterns]
    return statistics.mean(l) <= error


def capacity(n: int, error: float) -> float:
    p_min = 1
    p_max = n  # Reasonable upper bound on the number of storable patterns.
    p_mean = int((p_min + p_max)/2)

    while p_max - p_min > 1:  # The binary search is more that two times fast in this case.
        if is_storable(n, error, p_mean):
            p_min = p_mean
        else:
            p_max = p_mean
        p_mean = int((p_min + p_max)/2)

    return p_mean / n


def mean_capacity(n: int, error: float) -> (float, float):
    bar = progressbar.ProgressBar()
    l = [capacity(n, error) for _ in bar(range(Config().nb_exp))]
    mean = statistics.mean(l)
    return mean, statistics.stdev(l, mean)


def run():
    print("Question 1.4")
    os.environ['Q_14_HOPFIELD_CONFIG'] = 'questions/q14.json'
    for N in Config().Ns:
        print("N = %s:" % N)
        (mean, std) = mean_capacity(N, Config().error)
        print("\u03bc=%s" % mean)
        print("\u03c3=%s\n" % std)
