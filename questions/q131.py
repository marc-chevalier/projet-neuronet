#!/usr/bin/env python3

from hopfield.hopfield_network import DeterministicAsynchronousHopfieldNetwork as HopfieldNetwork
from hopfield.hopfield_network import TraversingMode
from hopfield.config import IntervalCConfig as Config
from hopfield.states import Pattern
import os
import numpy
import statistics
import progressbar


def calc(c: float) -> numpy.float64:
    patterns = Pattern.generate_random_patterns(Config().N, Config().P)
    net = HopfieldNetwork(patterns, TraversingMode.Default)
    return net.mean_retrieval_error(patterns[0], c)


def mean_calc(c: float) -> (float, float):
    nb_exp = Config().nb_exp
    l = [calc(c) for _ in range(nb_exp)]
    mean = statistics.mean(l)
    return mean, statistics.stdev(l, mean)


def run() -> None:
    print("Question 1.3.1")
    os.environ['INTERVAL_C_HOPFIELD_CONFIG'] = 'questions/q131.json'

    open('questions/q131.dat', 'w').close()
    file = os.open("questions/q131.dat", os.O_WRONLY | os.O_CREAT)

    os.write(file, b"c error std\n")

    bar = progressbar.ProgressBar()

    for c in bar(numpy.arange(Config().c_min,  Config().c_max + Config().c_step, Config().c_step)):
        os.write(file, ("%s " % c).encode('utf-8') + b" " + ("%s %s\n" % mean_calc(c)).encode('utf-8'))

    os.close(file)

    os.popen("gnuplot -persist questions/q131.p")
