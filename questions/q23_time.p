reset

set term pdfcairo
set output "questions/q23_time.pdf"

set title "time=f(P)"
set xlabel "P"
set ylabel "time"
set key out vert
set key center right

set style data linespoints

set pointsize 0.5   # la taille des points

plot "questions/q23_time.dat" using 1:2:3 title columnheader(2) with yerrorbars, \
     "questions/q23_time.dat" using 1:4:5 title columnheader(4) with yerrorbars, \
     "questions/q23_time.dat" using 1:6:7 title columnheader(6) with yerrorbars
