#!/usr/bin/env python3

from hopfield.hopfield_network import DeterministicAsynchronousHopfieldNetwork as HopfieldNetwork
from hopfield.hopfield_network import TraversingMode
from hopfield.config import IntervalPConfig as Config
from hopfield.states import Pattern
import os
import numpy
import statistics
import progressbar


def calc(p: int) -> numpy.float64:
    patterns = Pattern.generate_random_patterns(Config().N, p)
    net = HopfieldNetwork(patterns, TraversingMode.Default)
    return net.mean_retrieval_error(patterns[0], Config().c)


def mean_calc(p: int) -> numpy.float64:
    nb_exp = Config().nb_exp
    l = [calc(p) for _ in range(nb_exp)]
    mean = statistics.mean(l)
    return mean, statistics.stdev(l, mean)


def run() -> None:
    print("Question 1.3.2")
    os.environ['INTERVAL_P_HOPFIELD_CONFIG'] = 'questions/q132.json'

    open('questions/q132.dat', 'w').close()
    file = os.open("questions/q132.dat", os.O_WRONLY | os.O_CREAT)

    os.write(file, b"P error std\n")

    bar = progressbar.ProgressBar()

    for c in bar(range(Config().P_min,  Config().P_max + Config().P_step, Config().P_step)):
        os.write(file, ("%s " % c).encode('utf-8') + b" " + ("%s %s\n" % mean_calc(c)).encode('utf-8'))

    os.close(file)

    os.popen("gnuplot -persist questions/q132.p")
