PYTHON=python3
LATEX=pdflatex

all: report/report.pdf

tests:
	$(PYTHON) run_tests.py

graphs:
	$(PYTHON) run.py

report/report.pdf:
	cd report && $(LATEX) -interaction=nonstopmode -shell-escape report.tex

.PHONY: all tests graphs

clean:
	rm -f report/*.aux
	rm -f report/*.log
	rm -f report/*.pyg
	rm -f report/*.pdf

zip:
	rm -f *.zip && zip -v chevalier_hopfield.zip hopfield/*.* questions/*.* report/report.pdf run.py run_pylint.sh run_tests.py tests/*.* Makefile .pylintrc
